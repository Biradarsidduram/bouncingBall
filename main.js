var rand = Math.floor(Math.random()*5 +1);
var i =1;
var par = document.getElementById('main');
 while(rand){
	var ball =document.createElement('canvas');
	ball.className = 'ball'+ i;
	ball.style.backgroundColor =  "rgba("+Math.floor(Math.random()*255 +1)+','+Math.floor(Math.random()*255 +1)+','+Math.floor(Math.random()*255 +1)+")";
	par.appendChild(ball);
	++i;
	rand--;
 }
var canvas = document.getElementsByTagName('canvas');
var howManyTimes = 100000;
i = 0;
var x_array = [];//x scale value;
var y_array = [];// y scale value;
for(var p =0;p<canvas.length;p++){
	var react = canvas[p].getBoundingClientRect();
	canvas[p].style.left = react.left+'px';
	canvas[p].style.top = react.top+'px';
	canvas[p].style.display = 'none';
	x_array[p] = Math.floor(Math.random()*3+1);
	y_array[p] = Math.floor(Math.random()*3+1);
}
function loop() {
			for(var p =0;p<canvas.length;p++){
				x_array[p] =leftBound(canvas[p],x_array[p]);  // get the new value if hit the boundary otherwise the same value is present. 
				y_array[p] =topBound(canvas[p],y_array[p]);     	   		 
				canvas[p].style.left = parseInt(canvas[p].style.left)+x_array[p]+'px'; 
				canvas[p].style.top = parseInt(canvas[p].style.top)+y_array[p]+'px'; 
			}
	i++;
	if( i < howManyTimes ){
		setTimeout( loop, 1 );
	}
}
function leftBound(arg,a){
  	var leftpos = parseInt(arg.style.left);	
	 	if(leftpos>=470)
		 	return -Math.floor(Math.random()*3+1);
	 	if(leftpos<=0)
		 	return Math.floor(Math.random()*3+1);
	return a;	
}
function topBound(arg,a){
  	var toppos = parseInt(arg.style.top);
		if(toppos <=0)
			return Math.floor(Math.random()*3+1);
		if(toppos>=470)
			return - Math.floor(Math.random()*3+1);
	return a;
}
var btn = document.getElementById('btn');
btn.addEventListener('click',function(){
	loop();
	this.style.display = 'none';
	for(var i = 0;i<canvas.length;i++){
		canvas[i].style.display = 'block';
	}
});